package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.View
import android.widget.*
import androidx.core.widget.TextViewCompat
import com.example.myapplication.R.id.*
import com.google.android.material.textfield.TextInputEditText

class  MainActivity : AppCompatActivity() {
    lateinit var text_name: TextView
    lateinit var name_edit: EditText
    lateinit var viewDay: TextView
    lateinit var calendar: CalendarView
    lateinit var videoView: VideoView
    private var mediaController: MediaController? = null
    lateinit var checkBox: CheckBox
    lateinit var linearLayout: LinearLayout
    lateinit var number: EditText
    lateinit var password: EditText


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        text_name = findViewById(R.id.User_name)
        name_edit = findViewById(R.id.User_edit)
        viewDay = findViewById(R.id.movieDay)
        calendar = findViewById(R.id.calendar)
        videoView = findViewById(R.id.videoView)
        checkBox = findViewById(R.id.checkbox)
        linearLayout = findViewById(R.id.linear)
        number = findViewById(R.id.phonenumber)
        password = findViewById(R.id.passwordUser)
        check()
        videoView.setVideoPath("android.resource//" + getPackageName() + "/" + R.raw.videoplayback)
        videoView.start()
        /*mediaController = MediaController(this)
        mediaController?.setAnchorView(videoView)
        videoView.setMediaController(mediaController)*/
        calendar.setOnDateChangeListener{view, year, month, dayOfMonth ->
            viewDay.text = "$dayOfMonth.${month+1}.$year"
        }
    }
    fun check()
    {
        checkBox.setOnCheckedChangeListener{buttonView, isCheked ->
            if (isCheked){
                linearLayout.visibility = View.VISIBLE
            }
            else{
                linearLayout.visibility = View.GONE
            }
        }
    }
    fun save(view: android.view.View) {
        if (name_edit.text.toString().isNotEmpty())
        {
            val name = name_edit.text.toString()
            text_name.text = "Добро пожаловать,$name"
            text_name.visibility = View.VISIBLE
        }
    }

    /*fun send(view: android.view.View)
    {
        if (number.text.isNotEmpty() && password.text.isNotEmpty())
        {
            Toast.makeText(this, text"Вы успешно зарегистрированы", Toast.LENGTH_SHORT).show()
        }
        else
        {
            Toast.makeText(this, text"Заполните все поля", Toast.LENGTH_SHORT).show()
        }
    }*/


}

